import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteDaserraComponent } from './site-daserra.component';

describe('SiteDaserraComponent', () => {
  let component: SiteDaserraComponent;
  let fixture: ComponentFixture<SiteDaserraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteDaserraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteDaserraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
