import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-adiantadas',
  templateUrl: './adiantadas.component.html',
  styleUrls: ['./adiantadas.component.scss']
})
export class AdiantadasComponent implements OnInit {
  dados: any = [
    {
      title: 'MOSAICO PONTA NEGRA',
      image1: './assets/imgs/empreendimento/pontanegra.jpg',
      image: './assets/imgs/empreendimento/logo-pontanegra.jpg',
      text: 'AV. Perimetral Thales Loureiros, s/n, Ponta Negra, Manaus - AM',
      link: 'pontanegra',
      tag: 'Obras Adiantas'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
