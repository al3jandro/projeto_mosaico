import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdiantadasComponent } from './adiantadas.component';

describe('AdiantadasComponent', () => {
  let component: AdiantadasComponent;
  let fixture: ComponentFixture<AdiantadasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdiantadasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdiantadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
