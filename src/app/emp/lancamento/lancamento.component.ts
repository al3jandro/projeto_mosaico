import { Component, OnInit } from '@angular/core';

@Component({
  selector: "app-lancamento",
  templateUrl: "./lancamento.component.html",
  styleUrls: ["./lancamento.component.scss"]
})
export class LancamentoComponent implements OnInit {
  dados: any = [
    {
      title: "ARARAQUARA-SP",
      image1: "./assets/imgs/empreendimento/noimages.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Em breve",
      link: "contato"
    },
    {
      title: "VINHEDO-SP",
      image1: "./assets/imgs/empreendimento/noimages.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Em breve",
      link: "contato"
    },
    {
      title: "CAMAÇARI-BA",
      image1: "./assets/imgs/empreendimento/noimages.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Em breve",
      link: "contato"
    },
    {
      title: "BETIM-MG",
      image1: "./assets/imgs/empreendimento/noimages.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Em breve",
      link: "contato"
    },
    {
      title: "MOGI DAS CRUZES-SP",
      image1: "./assets/imgs/empreendimento/noimages.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Em breve",
      link: "contato"
    },
    {
      title: "MANAUS-AM",
      image1: "./assets/imgs/empreendimento/noimages.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Em breve",
      link: "contato"
    },
    {
      title: "COTIA-SP",
      image1: "./assets/imgs/empreendimento/noimages.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Em breve",
      link: "contato"
    },
    {
      title: "SUZANO-SP",
      image1: "./assets/imgs/empreendimento/noimages.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Em breve",
      link: "contato"
    },
    {
      title: "ARARAQUARA-SP",
      image1: "./assets/imgs/empreendimento/noimages.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Em breve",
      link: "contato"
    }
  ];

  constructor() {}

  ngOnInit() {}
}
