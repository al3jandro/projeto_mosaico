import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltanaComponent } from './altana.component';

describe('AltanaComponent', () => {
  let component: AltanaComponent;
  let fixture: ComponentFixture<AltanaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltanaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
