import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CachoerinhaComponent } from './cachoerinha.component';

describe('CachoerinhaComponent', () => {
  let component: CachoerinhaComponent;
  let fixture: ComponentFixture<CachoerinhaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CachoerinhaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CachoerinhaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
