import { Component, OnInit } from '@angular/core';

@Component({
  selector: "app-outros",
  templateUrl: "./outros.component.html",
  styleUrls: ["./outros.component.scss"]
})
export class OutrosComponent implements OnInit {
  dados: any = [
    {
      title: "360 JK",
      image1: "./assets/imgs/empreendimento/img360jk.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Av. Juscelino Kubitschek, 360 - Itaim - São Paulo - SP",
      link: "360"
    },
    {
      title: "AFIFE",
      image1: "./assets/imgs/empreendimento/imgafife.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Av. Angélica, 2029 - Higienópolis - São Paulo - SP",
      link: "afife"
    },
    {
      title: "ALTANA",
      image1: "./assets/imgs/empreendimento/imgaltana.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text:
        "Av. Republica do Libano, 1714 - Vila Nova Conceição - São Paulo - SP",
      link: "altana"
    },
    {
      title: "ANHANGUERA 107",
      image1: "./assets/imgs/empreendimento/imganhanguera.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Km 107 Rod. Anhanguera - Sumaré",
      link: "anhanguera"
    },
    {
      title: "POÈME CIDADE JARDIM",
      image1: "./assets/imgs/empreendimento/imgpoeme.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Rua Joaquim Macedo, 95 - Cidade Jadim - São Paulo - SP",
      link: "poeme"
    },
    {
      title: "TERRA MORENA",
      image1: "./assets/imgs/empreendimento/imgmorena.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "R. Marco Feliz - Campo Grande - MS",
      link: "morena"
    },
    {
      title: "CACHOEIRINHA BUSINESS PARK",
      image1: "./assets/imgs/empreendimento/imgcachoerinha.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text:
        "Av. Frederico Augusto Ritter, 7400 - Industrial, Cachoeirinha - RS",
      link: "cachoerinha"
    }
  ];

  constructor() {}

  ngOnInit() {}
}
