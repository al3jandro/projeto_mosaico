import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoemeComponent } from './poeme.component';

describe('PoemeComponent', () => {
  let component: PoemeComponent;
  let fixture: ComponentFixture<PoemeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoemeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoemeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
