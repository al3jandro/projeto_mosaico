import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnhangueraComponent } from './anhanguera.component';

describe('AnhangueraComponent', () => {
  let component: AnhangueraComponent;
  let fixture: ComponentFixture<AnhangueraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnhangueraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnhangueraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
