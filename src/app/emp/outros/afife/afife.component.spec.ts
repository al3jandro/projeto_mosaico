import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AfifeComponent } from './afife.component';

describe('AfifeComponent', () => {
  let component: AfifeComponent;
  let fixture: ComponentFixture<AfifeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AfifeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AfifeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
