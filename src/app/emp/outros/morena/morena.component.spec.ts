import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MorenaComponent } from './morena.component';

describe('MorenaComponent', () => {
  let component: MorenaComponent;
  let fixture: ComponentFixture<MorenaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MorenaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MorenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
