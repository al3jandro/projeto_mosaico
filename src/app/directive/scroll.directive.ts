import { Directive, Inject, HostListener } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';


@Directive({
  selector: '[appScroll]'
})
export class ScrollDirective {
  navIsFixed: boolean;
  constructor(@Inject(DOCUMENT) private document: Document) {
    this.scrollToTop();
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (
      window.pageYOffset ||
      document.documentElement.scrollTop ||
      document.body.scrollTop > 100
    ) {
      this.navIsFixed = true;
    } else if (
      (this.navIsFixed && window.pageYOffset) ||
      document.documentElement.scrollTop ||
      document.body.scrollTop < 10
    ) {
      this.navIsFixed = false;
    }
  }
  scrollToTop() {
    (function smoothscroll() {
      const currentScroll =
        document.documentElement.scrollTop || document.body.scrollTop;
      if (currentScroll > 0) {
        window.requestAnimationFrame(smoothscroll);
        window.scrollTo(0, currentScroll - currentScroll / 5);
      }
    })();
  }
}
