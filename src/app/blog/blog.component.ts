import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})


export class BlogComponent implements OnInit {
  feeds: any;
  url = '//projetomosaico.com.br/wp/wp-json/wp/v2/posts';
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getJson();
  }

  getJson() {
    this.http.get(this.url).subscribe(
      data => {
        this.feeds = data;
        console.log(data);
      }
    );
  }
}
