import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaBlogComponent } from './media-blog.component';

describe('MediaBlogComponent', () => {
  let component: MediaBlogComponent;
  let fixture: ComponentFixture<MediaBlogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaBlogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaBlogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
