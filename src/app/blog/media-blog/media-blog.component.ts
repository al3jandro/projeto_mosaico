import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { Meta } from '@angular/platform-browser';
import { MetaService } from 'ng2-meta';


@Component({
  selector: "app-media-blog",
  templateUrl: "./media-blog.component.html",
  styleUrls: ["./media-blog.component.scss"]
})
export class MediaBlogComponent implements OnInit {
  id: string;
  feed: any;
  public url = "//projetomosaico.com.br/wp/wp-json/wp/v2/posts";

  url2: any = "//projetomosaico.com.br";

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private spinner: NgxSpinnerService,
    private meta: Meta,
    private metaService: MetaService
  ) {
    this.route.url.subscribe(u => {
      this.id = this.route.snapshot.params.id;
      this.spinner.show();
    });
  }

  ngOnInit() {
    this.getProperty(this.id).subscribe(data => {
      this.feed = data;

      this.spinner.hide();
    });
  }

  getPost() {
    this.getProperty(this.id).subscribe(data => {
      this.feed = data;
      this.spinner.hide();
    });
  }

  private getProperty(id) {
    return this.http.get(`${this.url}?slug=${id}`);
  }

  // private tags(data) {
  //   this.meta.updateTag({ name: 'twitter:card', content: 'summary' });
  //   this.meta.updateTag({ name: 'twitter:site', content: 'Projeto Mosaico' });
  //   this.meta.updateTag({
  //     name: 'twitter:title',
  //     content: data[0].title.rendered
  //   });
  //   this.meta.updateTag({
  //     name: 'twitter:description',
  //     content: data[0].excerpt.rendered
  //   });
  //   this.meta.updateTag({
  //     name: 'twitter:image',
  //     content: data[0].fimg_url
  //   });
  //   this.meta.updateTag({ property: 'og:type', content: 'article' });
  //   this.meta.updateTag({ property: 'og:site_name', content: 'La Vitta' });
  //   this.meta.updateTag({
  //     property: 'og:title',
  //     content: data[0].title.rendered
  //   });
  //   this.meta.updateTag({
  //     property: 'og:description',
  //     content: data[0].excerpt.rendered
  //   });
  //   this.meta.updateTag({
  //     property: 'og:image',
  //     content: data[0].fimg_url
  //   });
  //   this.meta.updateTag({
  //     property: 'og:url',
  //     content: `${this.url2}/blog/${data[0].slug}`
  //   });
  // }

  private serviceTags(data) {
    this.metaService.setTitle(data[0].title.rendered);
    this.metaService.setTag("og:image", data[0].fimg_url);
    this.metaService.setTag("og:title", data[0].title.rendered);
    this.metaService.setTag("og:url", `${this.url2}/blog/${data[0].slug}`);
    this.metaService.setTag("og:description", data[0].excerpt.rendered);
  }
}
