import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.scss']
})
export class StoryComponent implements OnInit {

  url = '//projetomosaico.com.br/wp/wp-json/wp/v2/posts?per_page=200';
  p = 1;
  flag: boolean;
  flag2: boolean;
  feeds: any;
  countSearch: number;
  item: string;


  constructor(
    private http: HttpClient,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.getJson();
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    }, 5000);
  }

  getJson() {
    this.flag = true;
    this.countSearch = 0;
    this.http.get(this.url).subscribe(
      data => {
        this.feeds = data;
      }
    );
  }

  voltar() {
    this.getJson();
    this.flag = true;
    this.item = '';
  }

  search() {
      this.http.get(this.url + '&search=' + this.item)
      .subscribe(data => {
        this.countSearch = Object.keys(data).length;
        if (this.countSearch > 0) {
          this.flag = true;
          this.feeds = data;
          console.log('flag: ', this.flag);
          console.log(data);
          console.log('Cant Post', this.countSearch);
        } else {
          this.flag = false;
          this.countSearch = 0;
          console.log(data);
          console.log('Cant Post', this.countSearch);
        }
      });
  }
}
