import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbaMateriaisComponent } from './aba-materiais.component';

describe('AbaMateriaisComponent', () => {
  let component: AbaMateriaisComponent;
  let fixture: ComponentFixture<AbaMateriaisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbaMateriaisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbaMateriaisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
