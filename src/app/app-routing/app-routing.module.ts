import { AbaMateriaisComponent } from './../aba-materiais/aba-materiais.component';
import { StoryComponent } from './../blog/story/story.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SiteHomeComponent } from '../site-home/site-home.component';
import { SiteSobreComponent } from '../site-sobre/site-sobre.component';
import { SiteEssenceComponent } from '../site-essence/site-essence.component';
import { SiteContatoComponent } from './../site-contato/site-contato.component';
import { SiteVintageComponent } from './../site-vintage/site-vintage.component';
import { SiteAldeidaComponent } from '../site-aldeida/site-aldeida.component';
import { SiteDaserraComponent } from '../site-daserra/site-daserra.component';
import { SiteLosAngelesComponent } from './../site-los-angeles/site-los-angeles.component';
import { SiteBrasilisComponent } from './../site-brasilis/site-brasilis.component';
import { AdiantadasComponent } from './../emp/adiantadas/adiantadas.component';
import { OutrosComponent } from '../emp/outros/outros.component';
import { ConstruirComponent } from '../emp/construir/construir.component';
import { LancamentoComponent } from './../emp/lancamento/lancamento.component';
import { JkComponent } from './../emp/outros/jk/jk.component';
import { AfifeComponent } from './../emp/outros/afife/afife.component';
import { AnhangueraComponent } from './../emp/outros/anhanguera/anhanguera.component';
import { PoemeComponent } from './../emp/outros/poeme/poeme.component';
import { MorenaComponent } from './../emp/outros/morena/morena.component';
import { CachoerinhaComponent } from './../emp/outros/cachoerinha/cachoerinha.component';
import { AltanaComponent } from './../emp/outros/altana/altana.component';
import { MediaBlogComponent } from '../blog/media-blog/media-blog.component';
import { SitePontanegraComponent } from '../site-pontanegra/site-pontanegra.component';
import { MetaGuard } from 'ng2-meta';
import { PrivacidadeComponent } from '../layout/privacidade/privacidade.component';


const routes: Routes = [
  {
    path: '',
    component: SiteHomeComponent,
    pathMatch: 'full',
    canActivate: [MetaGuard],
    data: {
      meta: {
        title: 'Projeto Mosaico',
        image: './assets/imgs/slide/header-mini.jpg'
      }
    }
  },
  {
    path: 'privacidade',
    component: PrivacidadeComponent
  },
  {
    path: 'sobre',
    component: SiteSobreComponent
  },
  {
    path: 'contato',
    component: SiteContatoComponent
  },
  {
    path: 'pontanegra',
    component: SitePontanegraComponent
  },
  {
    path: 'essence',
    component: SiteEssenceComponent
  },
  {
    path: 'vintage',
    component: SiteVintageComponent
  },
  {
    path: 'aldeia',
    component: SiteAldeidaComponent
  },
  {
    path: 'daserra',
    component: SiteDaserraComponent
  },
  {
    path: 'brasilis',
    component: SiteBrasilisComponent
  },
  {
    path: 'losangeles',
    component: SiteLosAngelesComponent
  },
  {
    path: 'adiantadas',
    component: AdiantadasComponent
  },
  {
    path: 'lancamento',
    component: LancamentoComponent
  },
  {
    path: 'outros',
    component: OutrosComponent
  },
  {
    path: 'construir',
    component: ConstruirComponent
  },
  {
    path: '360',
    component: JkComponent
  },
  {
    path: 'afife',
    component: AfifeComponent
  },
  {
    path: 'altana',
    component: AltanaComponent
  },
  {
    path: 'anhanguera',
    component: AnhangueraComponent
  },
  {
    path: 'poeme',
    component: PoemeComponent
  },
  {
    path: 'morena',
    component: MorenaComponent
  },
  {
    path: 'cachoerinha',
    component: CachoerinhaComponent
  },
  {
    path: 'blog',
    component: StoryComponent
  },
  {
    path: 'blog/:id',
    component: MediaBlogComponent
  },
  {
    path: 'materiais',
    component: AbaMateriaisComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }