import { Component, ViewChild, HostListener, OnInit } from '@angular/core';
import { RdStationService } from './../rd-station.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-site-home',
  templateUrl: './site-home.component.html',
  styleUrls: ['./site-home.component.scss']
})
export class SiteHomeComponent implements OnInit {

  registerE: FormGroup;
  registerPN: FormGroup;
  submitted = false;

  constructor(
    private rdStation: RdStationService,
    private toast: ToastrService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.registerPN = this.formBuilder.group({
      nome: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      profissao: ['', Validators.required]
    });
    this.registerE = this.formBuilder.group({
      nome: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      profissao: ['', Validators.required]
    });
  }

  get f() { return this.registerPN.controls; }
  get e() { return this.registerE.controls; }


  essence() {
    this.submitted = true;
    if (this.registerE.invalid) {
      return;
    }
    this.rdStation.agenda(this.registerE.value, 'Formulario Home Site Essence')
    .subscribe(() => {
        this.toast.success(
          'Em breve, um consultor entrará em contato com você',
          'Obrigado'
        );
      },
      error => {
        console.log('Error', error);
      }
    );
  }

  pontanegra() {
    this.submitted = true;
    if (this.registerPN.invalid) {
      return;
    }
    this.rdStation.agenda(this.registerPN.value, 'Formulario Home Site Ponta Negra')
    .subscribe(() => {
        this.toast.success(
          'Em breve, um consultor entrará em contato com você',
          'Obrigado'
        );
      },
      error => {
        console.log('Error', error);
      }
    );
  }
}
