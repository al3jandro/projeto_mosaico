import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteVintageComponent } from './site-vintage.component';

describe('SiteVintageComponent', () => {
  let component: SiteVintageComponent;
  let fixture: ComponentFixture<SiteVintageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteVintageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteVintageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
