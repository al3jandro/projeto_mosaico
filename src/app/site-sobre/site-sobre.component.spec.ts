import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteSobreComponent } from './site-sobre.component';

describe('SiteSobreComponent', () => {
  let component: SiteSobreComponent;
  let fixture: ComponentFixture<SiteSobreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteSobreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteSobreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
