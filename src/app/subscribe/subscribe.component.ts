import { Component, OnInit } from '@angular/core';
import { RdStationService } from './../rd-station.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.scss']
})
export class SubscribeComponent implements OnInit {
  subscribeForm: FormGroup;
  submittedForm = false;
  news: any = { nome: '', email: '' };
  constructor(
    private rdStation: RdStationService,
    private toast: ToastrService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.subscribeForm = this.formBuilder.group({
      nome: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    });
  }

  get s() {
    return this.subscribeForm.controls;
  }

  save() {
    this.submittedForm = true;
    if (this.subscribeForm.invalid) {
      return;
    }
    this.rdStation.newsletter(this.subscribeForm.value).subscribe(
      () => {
        this.toast.success(
          'Em breve você receberá notificações para o seu e-mail',
          'Obrigado'
        );
      },
      error => {
        console.log('Error', error);
      }
    );
  }
}
