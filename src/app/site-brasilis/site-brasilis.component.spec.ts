import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteBrasilisComponent } from './site-brasilis.component';

describe('SiteBrasilisComponent', () => {
  let component: SiteBrasilisComponent;
  let fixture: ComponentFixture<SiteBrasilisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteBrasilisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteBrasilisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
