import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteAldeidaComponent } from './site-aldeida.component';

describe('SiteAldeidaComponent', () => {
  let component: SiteAldeidaComponent;
  let fixture: ComponentFixture<SiteAldeidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteAldeidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteAldeidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
