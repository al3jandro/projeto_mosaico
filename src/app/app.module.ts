import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AgmCoreModule } from '@agm/core';
import { MDBBootstrapModule } from './../../angular-bootstrap-md';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxPaginationModule } from 'ngx-pagination';
import { MetaModule } from 'ng2-meta';
import { NgcCookieConsentModule, NgcCookieConsentConfig } from 'ngx-cookieconsent';


import { AppComponent } from './app.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { EmpreedimentoComponent } from './empreedimento/empreedimento.component';
import { BlogComponent } from './blog/blog.component';
import { ProjetoComponent } from './projeto/projeto.component';
import { FooterComponent } from './footer/footer.component';
import { MapsComponent } from './maps/maps.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { SiteHomeComponent } from './site-home/site-home.component';
import { SiteSobreComponent } from './site-sobre/site-sobre.component';
import { SiteEssenceComponent } from './site-essence/site-essence.component';
import { SitePontanegraComponent } from './site-pontanegra/site-pontanegra.component';
import { SiteContatoComponent } from './site-contato/site-contato.component';
import { HeaderMiniComponent } from './header-mini/header-mini.component';
import { NavComponent } from './nav/nav.component';
import { SiteVintageComponent } from './site-vintage/site-vintage.component';
import { SiteAldeidaComponent } from './site-aldeida/site-aldeida.component';
import { SiteDaserraComponent } from './site-daserra/site-daserra.component';
import { SiteBrasilisComponent } from './site-brasilis/site-brasilis.component';
import { SiteLosAngelesComponent } from './site-los-angeles/site-los-angeles.component';
import { AdiantadasComponent } from './emp/adiantadas/adiantadas.component';
import { ConstruirComponent } from './emp/construir/construir.component';
import { LancamentoComponent } from './emp/lancamento/lancamento.component';
import { OutrosComponent } from './emp/outros/outros.component';
import { JkComponent } from './emp/outros/jk/jk.component';
import { AfifeComponent } from './emp/outros/afife/afife.component';
import { AltanaComponent } from './emp/outros/altana/altana.component';
import { AnhangueraComponent } from './emp/outros/anhanguera/anhanguera.component';
import { PoemeComponent } from './emp/outros/poeme/poeme.component';
import { MorenaComponent } from './emp/outros/morena/morena.component';
import { CachoerinhaComponent } from './emp/outros/cachoerinha/cachoerinha.component';
import { StoryComponent } from './blog/story/story.component';
import { MediaBlogComponent } from './blog/media-blog/media-blog.component';
import { AbaMateriaisComponent } from './aba-materiais/aba-materiais.component';
import { EbookComponent } from './ebook/ebook.component';
import { BannerSidebarComponent } from './banner-sidebar/banner-sidebar.component';

import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { TruncatePipe } from './pipe/truncate.pipe';
import { ScrollDirective } from './directive/scroll.directive';
import { SanitizePipe } from './pipe/sanitize.pipe';
import { SafePipe } from './pipe/safe.pipe';
import { PrivacidadeComponent } from './layout/privacidade/privacidade.component';



declare var Hammer: any;

export class MyHammerConfig extends HammerGestureConfig {
  buildHammer(element: HTMLElement) {
    const mc = new Hammer(element, {
      touchAction: 'auto',
      inputClass: Hammer.SUPPORT_POINTER_EVENTS ? Hammer.PointerEventInput : Hammer.TouchInput,
      recognizers: [
        [Hammer.Swipe, {
          direction: Hammer.DIRECTION_HORIZONTAL
        }]
      ],
    });
    return mc;
  }
  overrides = <any>{
    'pan': { direction: Hammer.DIRECTION_All },
    'swipe': { direction: Hammer.DIRECTION_VERTICAL }
  }
}

const cookieConfig: NgcCookieConsentConfig = {
  cookie: {
    domain: 'https://projetomosaico.com.br'
  },
  position: 'bottom',
  theme: 'classic',
  palette: {
    popup: {
      background: '#000000',
      text: '#ffffff',
      link: '#ffffff'
    },
    button: {
      background: '#f1d600',
      text: '#000000',
      border: 'transparent'
    }
  },
  type: 'info',
  content: {
    message:
      'Ao visitar nosso site, você estará concordando com nossas políticas de segurança e privacidade.',
    dismiss: ' De acordo',
    deny: 'Recusar cookies',
    link: 'Saber mais',
    href: 'https://projetomosaico.com.br/privacidade'
  }
};

@NgModule({
  declarations: [
    AppComponent,
    SubscribeComponent,
    EmpreedimentoComponent,
    BlogComponent,
    ProjetoComponent,
    FooterComponent,
    MapsComponent,
    SiteHomeComponent,
    SiteSobreComponent,
    SitePontanegraComponent,
    SiteEssenceComponent,
    SiteContatoComponent,
    HeaderMiniComponent,
    NavComponent,
    SiteVintageComponent,
    SiteAldeidaComponent,
    SiteDaserraComponent,
    SiteBrasilisComponent,
    SiteLosAngelesComponent,
    AdiantadasComponent,
    ConstruirComponent,
    LancamentoComponent,
    OutrosComponent,
    JkComponent,
    AfifeComponent,
    AltanaComponent,
    AnhangueraComponent,
    PoemeComponent,
    MorenaComponent,
    CachoerinhaComponent,
    StoryComponent,
    MediaBlogComponent,
    AbaMateriaisComponent,
    EbookComponent,
    BannerSidebarComponent,
    TruncatePipe,
    ScrollDirective,
    SanitizePipe,
    SafePipe,
    PrivacidadeComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAHAW2QhYaIfY68THlu-wn4sBcTvM0KGII'
    }),
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
    NgxPaginationModule,
    MetaModule.forRoot(),
    NgcCookieConsentModule.forRoot(cookieConfig)
  ],
  providers: [
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: MyHammerConfig
    }
  ],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }