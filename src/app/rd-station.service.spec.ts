import { TestBed, inject } from '@angular/core/testing';

import { RdStationService } from './rd-station.service';

describe('RdStationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RdStationService]
    });
  });

  it('should be created', inject([RdStationService], (service: RdStationService) => {
    expect(service).toBeTruthy();
  }));
});
