import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RdStationService {

  config: any;
  constructor(public http: HttpClient) {
    this.config = {
      token: 'fd57ed7d8a8effad7ed6ae15bf95edf8',
      url: 'https://www.rdstation.com.br/api/1.3/conversions'
    };
  }

  public newsletter(items: any) {
    items = {
      'token_rdstation' : this.config.token,
      'identificador' : 'NewsLetter',
      'Nome' : items.nome,
      'email' : items.email
    };
    return this.http.post(
      this.config.url,
      items,
      {
        headers: { 'Content-Type': 'application/json; charset=utf-8'}
      }
    );
  }

  public contato(items: any) {
    items = {
      'token_rdstation' : this.config.token,
      'identificador' : 'Fale Conosco',
      'Nome' : items.nome,
      'email' : items.email,
      'Telefone': items.telefone,
      'Empreendimento': items.empreendimento,
      'Assunto' : items.assunto,
      'mensagem': items.mensagem
    };
    return this.http.post(
      this.config.url,
      items,
      {
        headers: { 'Content-Type': 'application/json; charset=utf-8'}
      }
    );
  }

  public agenda(items: any, identificador: string) {
    items = {
      'token_rdstation': this.config.token,
      'identificador': identificador,
      'Nome': items.nome,
      'email': items.email,
      'Telefone': items.phone,
      'Profissão': items.profissao
    };
    return this.http.post(
      this.config.url,
      items,
      {
        headers: { 'Content-Type': 'application/json; charset=utf-8' }
      }
    );
  }

}