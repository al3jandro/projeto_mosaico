import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteLosAngelesComponent } from './site-los-angeles.component';

describe('SiteLosAngelesComponent', () => {
  let component: SiteLosAngelesComponent;
  let fixture: ComponentFixture<SiteLosAngelesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteLosAngelesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteLosAngelesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
