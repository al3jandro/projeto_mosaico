import { Component, OnInit } from '@angular/core';

@Component({
  selector: "app-projeto",
  templateUrl: "./projeto.component.html",
  styleUrls: ["./projeto.component.scss"]
})
export class ProjetoComponent implements OnInit {
  dados: any = [
    {
      title: "ARARAQUARA-SP",
      image1: "./assets/imgs/empreendimento/noimages.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Em breve",
      link: "contato"
    },
    {
      title: "VINHEDO-SP",
      image1: "./assets/imgs/empreendimento/noimages.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Em breve",
      link: "contato"
    },
    {
      title: "CAMAÇARI-BA",
      image1: "./assets/imgs/empreendimento/noimages.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Em breve",
      link: "contato"
    }
  ];
  dados2: any = [
    {
      title: "BETIM-MG",
      image1: "./assets/imgs/empreendimento/noimages.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Em breve",
      link: "contato"
    },
    {
      title: "MOGI DAS CRUZES-SP",
      image1: "./assets/imgs/empreendimento/noimages.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Em breve",
      link: "contato"
    },
    {
      title: "MANAUS-AM",
      image1: "./assets/imgs/empreendimento/noimages.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Em breve",
      link: "contato"
    }
  ];
  dados3: any = [
    {
      title: "COTIA-SP",
      image1: "./assets/imgs/empreendimento/noimages.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Em breve",
      link: "contato"
    },
    {
      title: "SUZANO-SP",
      image1: "./assets/imgs/empreendimento/noimages.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Em breve",
      link: "contato"
    },
    {
      title: "ARARAQUARA-SP",
      image1: "./assets/imgs/empreendimento/noimages.jpg",
      image: "./assets/imgs/empreendimento/logo-empre.jpg",
      text: "Em breve",
      link: "contato"
    }
  ];
  constructor() {}

  ngOnInit() {}
}
