import { Component, OnInit } from '@angular/core';

@Component({
  selector: "app-banner-sidebar",
  templateUrl: "./banner-sidebar.component.html",
  styleUrls: ["./banner-sidebar.component.scss"]
})
export class BannerSidebarComponent implements OnInit {
  dados: any = [
    {
      title: "15 dicas de Segurança Domiciliar",
      image: "./assets/imgs/banner-ebook-min.jpg",
      link: "https://conteudo.projetomosaico.com.br/seguranca-domiciliar"
    },
    {
      title: "Do que você precisa para ter qualidade de vida?",
      image: "./assets/imgs/banner-ebook2-min.jpg",
      link: "https://projetomosaico.com.br/simulador"
    }
  ];

  constructor() {}

  ngOnInit() {}
}
