import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerSidebarComponent } from './banner-sidebar.component';

describe('BannerSidebarComponent', () => {
  let component: BannerSidebarComponent;
  let fixture: ComponentFixture<BannerSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BannerSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
