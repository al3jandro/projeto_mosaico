import { Component, OnInit } from '@angular/core';
import { CarouselModule, WavesModule } from 'angular-bootstrap-md';

@Component({
  selector: 'app-empreedimento',
  templateUrl: './empreedimento.component.html',
  styleUrls: ['./empreedimento.component.scss']
})
export class EmpreedimentoComponent implements OnInit {
  dados: any = [
    {
      title: 'MOSAICO PONTA NEGRA',
      image1: './assets/imgs/empreendimento/pontanegra.jpg',
      image: './assets/imgs/empreendimento/logo-pontanegra.jpg',
      text: 'AV. Perimetral Thales Loureiros, s/n, Ponta Negra, Manaus - AM',
      link: 'pontanegra',
      tag: 'Obras Adiantas'
    },
    {
      title: 'MOSAICO ESSENCE',
      image1: './assets/imgs/empreendimento/essence.jpg',
      image: './assets/imgs/empreendimento/logo-essence.jpg',
      text:
        'AV. Presidente Castelo Branco, 3003 - Cesar de Souza, Mogi das Cruzes - SP',
      link: 'essence',
      tag: 'Pronto para construir'
    },
    {
      title: 'VINTAGE',
      image1: './assets/imgs/empreendimento/vintage.jpg',
      image: './assets/imgs/empreendimento/logo-vintage.jpg',
      text: 'Estrada do Capuava, 4570, Granja Viana Cotia - SP',
      link: 'vintage',
      tag: 'Pronto para construir'
    }
  ];
  dados2: any = [
    {
      title: 'MOSAICO ALDEIA',
      image1: './assets/imgs/empreendimento/aldeia.jpg',
      image: './assets/imgs/empreendimento/logo-aldeia.jpg',
      text: 'Estrada do Agrônomo, 759, Aldeia da Serra, Santana de Parnaíba.',
      link: 'aldeia',
      tag: 'Pronto para construir'
    },
    {
      title: 'MOSAICO DA SERRA',
      image1: './assets/imgs/empreendimento/serra.jpg',
      image: './assets/imgs/empreendimento/logo-serra.jpg',
      text:
        'AV. Prefeito Francisco Nogueira, 2314 - Km 57,5 da Rodovia Mogi-Bertioga.',
      link: 'daserra',
      tag: 'Pronto para construir'
    },
    {
      title: 'TERRA BRASILIS',
      image1: './assets/imgs/empreendimento/brasilis.jpg',
      image: './assets/imgs/empreendimento/logo-brasilis.jpg',
      text: 'Endereço: Av. Emílio Chechinato, 3400 - Itupeva - São Paulo - SP.',
      link: 'brasilis',
      tag: 'Pronto para construir'
    }
  ];
  dados3: any = [
    {
      title: 'VILLAGE LOS ANGELES',
      image1: './assets/imgs/empreendimento/losangeles.jpg',
      image: './assets/imgs/empreendimento/logo-losangeles.jpg',
      text: 'Estrada do Embú, 2153, Granja Viana. Cotia - SP',
      link: 'losangeles',
      tag: 'Pronto para construir'

    },
    {
      title: 'MOSAICO PONTA NEGRA',
      image1: './assets/imgs/empreendimento/pontanegra.jpg',
      image: './assets/imgs/empreendimento/logo-pontanegra.jpg',
      text: 'AV. Perimetral Thales Loureiros, s/n, Ponta Negra, Manaus - AM',
      link: 'pontanegra',
      tag: 'Obras Adiantas'
    },
    {
      title: 'MOSAICO ESSENCE',
      image1: './assets/imgs/empreendimento/essence.jpg',
      image: './assets/imgs/empreendimento/logo-essence.jpg',
      text:
        'AV. Presidente Castelo Branco, 3003 - Cesar de Souza, Mogi das Cruzes - SP',
      link: 'essence',
      tag: 'Pronto para construir'
    }
  ];

  constructor() {}

  ngOnInit() {}

}
