import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpreedimentoComponent } from './empreedimento.component';

describe('EmpreedimentoComponent', () => {
  let component: EmpreedimentoComponent;
  let fixture: ComponentFixture<EmpreedimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpreedimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpreedimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
