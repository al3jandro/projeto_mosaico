import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteContatoComponent } from './site-contato.component';

describe('SiteContatoComponent', () => {
  let component: SiteContatoComponent;
  let fixture: ComponentFixture<SiteContatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteContatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteContatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
