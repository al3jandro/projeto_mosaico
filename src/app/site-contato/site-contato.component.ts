import { Component, OnInit } from '@angular/core';
import { RdStationService } from './../rd-station.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: "app-site-contato",
  templateUrl: "./site-contato.component.html",
  styleUrls: ["./site-contato.component.scss"]
})
export class SiteContatoComponent implements OnInit {
  news: any = { nome: "", email: "", telefone: "", mensagem: "" };
  optionsAssunto: Array<any>;
  optionsEmp: Array<any>;
  optionsLan: Array<any>;
  
  constructor(
    private rdStation: RdStationService,
    private toast: ToastrService
  ) {}

  ngOnInit() {
    this.optionsAssunto = [
      { value: "Reclamaçoes", label: "Reclamaçoes" },
      { value: "Dúvidas", label: "Dúvidas" },
      { value: "Outros", label: "Outros" }
    ];
    this.optionsEmp = [
      {
        value: "Mosaico Essence - Mogi das Cruzes/SP",
        label: "Mosaico Essence - Mogi das Cruzes/SP"
      },
      {
        value: "Mosaico da Aldeia - Santana de Parnaiba/SP",
        label: "Mosaico da Aldeia - Santana de Parnaiba/SP"
      },
      {
        value: "Mosaico da Serra - Mogi das Cruzes/SP",
        label: "Mosaico da Serra - Mogi das Cruzes/SP"
      },
      {
        value: "Terra Brasilis - Itupeva/SP",
        label: "Terra Brasilis - Itupeva/SP"
      },
      {
        value: "Mosaico Ponta Negra - Manaus/AM",
        label: "Mosaico Ponta Negra - Manaus/AM"
      },
      {
        value: "Village Los Angeles - Cotia/SP",
        label: "Village Los Angeles - Cotia/SP"
      },
      { value: "Vintage - Cotia/SP", label: "Vintage - Cotia/SP" }
    ];
    this.optionsLan = [
      { value: "Araraquara/SP", label: "Araraquara/SP" },
      { value: "Vinhedo/SP", label: "Vinhedo/SP" },
      { value: "Camaçari/BA", label: "Camaçari/BA" },
      { value: "Betim/MG", label: "Betim/MG" },
      { value: "Mogi da Cruzes/SP", label: "Mogi da Cruzes/SP" },
      { value: "Manaus/AM", label: "Manaus/AM" },
      { value: "Cotia/SP", label: "Cotia/SP" },
      { value: "Suzano/SP", label: "Suzano/SP" }
    ];
  }

  save() {
    if(this.news.email === '') {
      this.toast.error("Preencha todos os campos", 'Erro');
    } else {
      this.rdStation.contato(this.news).subscribe(
        () => {
          this.toast.success(
            "Em breve, um consultor entrará em contato com você",
            "Obrigado"
          );
          this.news = "";
        },
        error => {
          console.log("Error", error);
        }
      );
    }
  }
}