import { Component, OnInit } from '@angular/core';

@Component({
  selector: "app-ebook",
  templateUrl: "./ebook.component.html",
  styleUrls: ["./ebook.component.scss"]
})
export class EbookComponent implements OnInit {
  dados: any = [
    {
      title: "15 dicas de Segurança Domiciliar",
      image1: "./assets/imgs/ebook/15_dicas_de_securanca_familiar.jpg",
      text:
        "Confira as principais dicas para preservar a segurança da sua família na sua casa!",
      link: "https://conteudo.projetomosaico.com.br/seguranca-domiciliar"
    },
    {
      title: "Saiba como comprar sua casa com tranquilidade.",
      image1: "./assets/imgs/ebook/saiba_como_comprar_sua_casa.jpg",
      text: "Não compre uma casa antes de ler o nosso E-book ",
      link:
        "https://conteudo.projetomosaico.com.br/como-comprar-sua-casa-com-tranquilidade"
    },
    {
      title: "Do que você precisa para ter qualidade de vida?",
      image1: "./assets/imgs/ebook/qualidade_de_vida.jpg",
      text:
        "Saiba como tomar as decisões essenciais para melhorar o seu modo de viver!",
      link:
        "https://conteudo.projetomosaico.com.br/guia-qualidade-de-vida-essence"
    }
  ];
  dados2: any = [
    {
      title: "Como aproveitar melhor o espaço do seu terreno?",
      image1:
        "./assets/imgs/ebook/aproveitar_melhor_o_espaco_do_seu_terreno.jpg",
      text: "Descubra a maneira ideal de planejar a construção do seu imóvel.",
      link:
        "https://conteudo.projetomosaico.com.br/como-aproveitar-o-espaco-do-seu-terreno"
    }
  ];

  constructor() {}

  ngOnInit() {}
}
