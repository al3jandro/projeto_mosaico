import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteEssenceComponent } from './site-essence.component';

describe('SiteEssenceComponent', () => {
  let component: SiteEssenceComponent;
  let fixture: ComponentFixture<SiteEssenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteEssenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteEssenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
