import { Component, OnInit } from '@angular/core';
import { RdStationService } from '../rd-station.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-site-essence',
  templateUrl: './site-essence.component.html',
  styleUrls: ['./site-essence.component.scss']
})
export class SiteEssenceComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  
  constructor(
    private rdStation: RdStationService,
    private toast: ToastrService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      nome: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      profissao: ['', Validators.required]
    });
  }

  get f() { return this.registerForm.controls; }

  essence() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.rdStation
    .agenda(this.registerForm.value, "Agenda Essence")
    .subscribe(
      () => {
        this.toast.success(
          "Em breve, um consultor entrará em contato com você",
          "Obrigado"
        );
      },
      error => {
        console.log("Error", error);
      }
    );
  }
}