import { Component, OnInit } from '@angular/core';
import { RdStationService } from './../rd-station.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-site-pontanegra',
  templateUrl: './site-pontanegra.component.html',
  styleUrls: ['./site-pontanegra.component.scss']
})
export class SitePontanegraComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;

  constructor(
    private rdStation: RdStationService,
    private toast: ToastrService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      nome: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      profissao: ['', Validators.required]
    });
  }

  get f() { return this.registerForm.controls; }

  pontanegra() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.rdStation
    .agenda(this.registerForm.value, "Agenda Ponta Negra")
    .subscribe(
      () => {
        this.toast.success(
          "Em breve, um consultor entrará em contato com você",
          "Obrigado"
        );
      },
      error => {
        console.log("Error", error);
      }
    );
  }
}
