import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SitePontanegraComponent } from './site-pontanegra.component';

describe('SitePontanegraComponent', () => {
  let component: SitePontanegraComponent;
  let fixture: ComponentFixture<SitePontanegraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SitePontanegraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SitePontanegraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
