import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss']
})
export class MapsComponent implements OnInit {
  public map: any = { lat: -3.05, lng: -60.09 };
  public map1: any = { lat: -3.4, lng: -60.8 };
  constructor() { }

  ngOnInit() {
  }
}
